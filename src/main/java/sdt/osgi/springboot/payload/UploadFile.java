package sdt.osgi.springboot.payload;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
public class UploadFile {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String originFileName;
    private String fileDownloadUri;
    private String fileType;
    private long size;

    public UploadFile( String originFileName, String fileDownloadUri, String fileType, long size) {
        this.originFileName = originFileName;
        this.fileDownloadUri = fileDownloadUri;
        this.fileType = fileType;
        this.size = size;
    }

    public UploadFile() {
    }
}
