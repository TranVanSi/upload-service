package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dao.UploadFileRepository;
import sdt.osgi.springboot.payload.UploadFile;
import sdt.osgi.springboot.payload.UploadFileDTO;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.Base64;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UploadFileService {
    private final FileStorageService fileStorageService;
    private final UploadFileRepository uploadFileRepository;

    public UploadFile add(UploadFile uploadFile) {
        return uploadFileRepository.save(uploadFile);
    }

    public UploadFile add(UploadFileDTO uploadFileDTO) {

        StringBuilder fileDownloadUri = new StringBuilder("/api/v1/upload/downloadFile/");
        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        String mimeType = fileNameMap.getContentTypeFor(uploadFileDTO.getTenFile());
        byte[] decodedBytes = Base64.getDecoder().decode(uploadFileDTO.getImageBase64());

        UploadFile uploadFile = add(new UploadFile(uploadFileDTO.getTenFile(), fileDownloadUri.toString(),
                mimeType, decodedBytes.length));
        InputStream is = new ByteArrayInputStream(decodedBytes);

        fileStorageService.storeFile(is, uploadFile.getId());

        return uploadFile;
    }

    public void delete(long fileId) {
        uploadFileRepository.deleteById(fileId);
    }

    public void addAll (List<UploadFile> uploadFiles) {
        uploadFileRepository.saveAll(uploadFiles);
    }

    public UploadFile getFileUpload (long fileId) {
        return uploadFileRepository.findById(fileId).get();
    }

    public UploadFile uploadHinhAnhKhaiBao(UploadFileDTO uploadFileDTO) {

        StringBuilder fileDownloadUri = new StringBuilder("/api/v1/uploadkhaibao/downloadFile/");
        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        String mimeType = fileNameMap.getContentTypeFor(uploadFileDTO.getTenFile());
        byte[] decodedBytes = Base64.getDecoder().decode(uploadFileDTO.getImageBase64());

        UploadFile uploadFile = add(new UploadFile(uploadFileDTO.getTenFile(), fileDownloadUri.toString(),
                mimeType, decodedBytes.length));
        InputStream is = new ByteArrayInputStream(decodedBytes);

        fileStorageService.storeFile(is, uploadFile.getId());

        return uploadFile;
    }
}
