package sdt.osgi.springboot.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.payload.UploadFile;

@Repository
public interface UploadFileRepository extends JpaRepository<UploadFile, Long> {

}
