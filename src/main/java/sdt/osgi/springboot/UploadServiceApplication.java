package sdt.osgi.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import sdt.osgi.springboot.property.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import javax.sql.DataSource;
import java.sql.SQLException;

@SpringBootApplication
@EnableConfigurationProperties({
		FileStorageProperties.class
})
public class UploadServiceApplication {

	@Autowired
	private Environment env;

	public static void main(String[] args) {
		SpringApplication.run(UploadServiceApplication.class, args);
	}

	@Bean(name = "dataSource")
	public DataSource getDataSource() throws SQLException {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();

		dataSource.setDriverClassName(env.getProperty("upload.datasource.driverClassName"));
		dataSource.setUrl(env.getProperty("upload.datasource.url"));
		dataSource.setUsername(env.getProperty("upload.datasource.username"));
		dataSource.setPassword(env.getProperty("upload.datasource.password"));

		return dataSource;
	}
}
