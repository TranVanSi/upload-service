package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sdt.osgi.springboot.payload.ResourceDTO;
import sdt.osgi.springboot.payload.UploadFile;
import sdt.osgi.springboot.payload.UploadFileDTO;
import sdt.osgi.springboot.service.FileStorageService;
import sdt.osgi.springboot.service.UploadFileService;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/upload")
public class FileController {

    private final UploadFileService uploadFileService;
    private static final Logger logger = LoggerFactory.getLogger(FileController.class);
    private final FileStorageService fileStorageService;

    @PostMapping("/uploadFile")
    public UploadFile uploadFile(@RequestParam("filex") MultipartFile file) {

        StringBuilder fileDownloadUri = new StringBuilder("/api/v1/upload/downloadFile/");

        UploadFile uploadFile = uploadFileService.add(new UploadFile(file.getOriginalFilename(), fileDownloadUri.toString(),
                file.getContentType(), file.getSize()));
        fileStorageService.storeFile(file, uploadFile.getId());

        return uploadFile;
    }

    @PostMapping
    public UploadFile uploadFile(@RequestBody UploadFileDTO uploadFileDTO) {

        return uploadFileService.add(uploadFileDTO);
    }

    @PostMapping("/uploadMultipleFiles")
    public List<UploadFile> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
        return Arrays.asList(files)
                .stream()
                .map(file -> uploadFile(file))
                .collect(Collectors.toList());
    }

    @GetMapping("/download/{fileId}")
    public ResourceDTO download(@PathVariable String fileId, HttpServletRequest request) throws IOException {
        ResourceDTO resourceDTO = new ResourceDTO();
        byte[] decodedBytes = Base64.getDecoder().decode(fileId);
        String decodedString = new String(decodedBytes);
        long id = 0l;
        if (decodedString != null) {
            id = Long.parseLong(decodedString);
        }

        UploadFile uploadFile = uploadFileService.getFileUpload(id);
        Resource resource = fileStorageService.loadFileAsResource(uploadFile != null ? uploadFile.getId() + "" : "");

        resourceDTO.setName(uploadFile.getOriginFileName());
        resourceDTO.setUrl(resource.getFile().getPath());
        return resourceDTO;
    }

    @DeleteMapping("/deleteFile/{fileId}")
    public void deleteFile (@PathVariable long fileId) throws IOException {
        UploadFile uploadFile = uploadFileService.getFileUpload(fileId);

        if (uploadFile != null) {
            Resource resource = fileStorageService.loadFileAsResource(uploadFile != null ? uploadFile.getId() + "" : "");
            resource.getFile().delete();

            uploadFileService.delete(fileId);
        }
    }
}
