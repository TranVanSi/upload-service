package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sdt.osgi.springboot.payload.UploadFile;
import sdt.osgi.springboot.payload.UploadFileDTO;
import sdt.osgi.springboot.service.FileStorageService;
import sdt.osgi.springboot.service.UploadFileService;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/uploadkhaibao")
public class FileKhaiBaoRestController {

    private final UploadFileService uploadFileService;
    private static final Logger logger = LoggerFactory.getLogger(FileKhaiBaoRestController.class);
    private final FileStorageService fileStorageService;

    @PostMapping("/uploadFile")
    public UploadFile uploadFile(@RequestParam("filex") MultipartFile file) {

        StringBuilder fileDownloadUri = new StringBuilder("/api/v1/uploadkhaibao/downloadFile/");

        UploadFile uploadFile = uploadFileService.add(new UploadFile(file.getOriginalFilename(), fileDownloadUri.toString(),
                file.getContentType(), file.getSize()));
        fileStorageService.storeFile(file, uploadFile.getId());

        return uploadFile;
    }

    @PostMapping
    public UploadFile uploadFile(@RequestBody UploadFileDTO uploadFileDTO) {

        return uploadFileService.uploadHinhAnhKhaiBao(uploadFileDTO);
    }

    @PostMapping("/uploadMultipleFiles")
    public List<UploadFile> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
        return Arrays.asList(files)
                .stream()
                .map(file -> uploadFile(file))
                .collect(Collectors.toList());
    }

    @GetMapping("/downloadFile/{fileId}")
    public ResponseEntity<Resource> downloadFile(@PathVariable long fileId, HttpServletRequest request) {
        UploadFile uploadFile = uploadFileService.getFileUpload(fileId);

        Resource resource = fileStorageService.loadFileAsResource(uploadFile != null ? uploadFile.getId() + "" : "");

        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + uploadFile.getOriginFileName() + "\"")
                .body(resource);
    }

    @DeleteMapping("/deleteFile/{fileId}")
    public void deleteFile (@PathVariable long fileId) throws IOException {
        UploadFile uploadFile = uploadFileService.getFileUpload(fileId);

        if (uploadFile != null) {
            Resource resource = fileStorageService.loadFileAsResource(uploadFile != null ? uploadFile.getId() + "" : "");
            resource.getFile().delete();

            uploadFileService.delete(fileId);
        }
    }

    @GetMapping("/viewFile/{fileId}")
    public String viewFile(@PathVariable long fileId, HttpServletRequest request) {
        UploadFile uploadFile = uploadFileService.getFileUpload(fileId);
        String encodedString = "";
        Resource resource = fileStorageService.loadFileAsResource(uploadFile != null ? uploadFile.getId() + "" : "");

        try {
            byte[] fileContent = FileUtils.readFileToByteArray(new File(resource.getFile().getAbsolutePath()));
            encodedString = Base64.getEncoder().encodeToString(fileContent);

        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

       return encodedString;
    }
}
